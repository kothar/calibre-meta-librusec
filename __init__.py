from calibre.ebooks.metadata.book.base import Metadata
from calibre.ebooks.metadata.sources.base import Source

from . import lre

class LibRusEcMetadataSourcePlugin(Source):
    version = (0, 0, 1)
    author = 'Dmitry Faleleev'
    name = 'lib.rus.ec'
    description = 'Looks up for book information on lib.rus.ec.'
    capabilities = frozenset(('identify',))
    supported_platforms = ('windows', 'osx', 'linux')
    touched_fields = frozenset(
        ['title', 'authors', 'identifier:lib.rus.ec', 'tags', 'series', 'series_index', 'languages'])
    cached_cover_url_is_reliable = False

    def is_customizable(self):
        return False

    def identify(self, log, result_queue, abort, title=None, authors=None, identifiers=None, timeout=30):
        log.debug(u'lib.rus.ec identification lookup started ...')

        books = lre.search(log, abort, title)

        idtype = 'librusec'

        for book_id in books:
            book = books[book_id]
            title = book['title']
            authors = book['authors']
            tags = book['tags']
            cover = book['cover']
            annotation = book['annotation']

            mi = Metadata(book['title'], book['authors'])
            mi.set_identifier(idtype, book_id)

            if tags:
                mi.tags = tags
            if cover:
                mi.has_cover = True
                mi.cover_data = cover
            if annotation:
                mi.comments = annotation

            result_queue.put(mi)

        log.debug(u'lib.rus.ec identification completed')
        return

# Plugin Install script
# calibre-customize -b .