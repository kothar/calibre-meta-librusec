import re
from lxml import html

import urllib.parse
import urllib.request

import logging

site = 'http://lib.rus.ec'
default_timeout = 30


def search(log, abort, title, timeout=default_timeout):
    book_urls = title_search(logging, title)
    log.info(book_urls)
    books = {}
    for url in book_urls:
        book_title = book_urls[url]
        book_metadata = fetch_book_info(log, url, book_title, timeout)
        books[url] = book_metadata

    return books


def title_search(log, title=None, timeout=default_timeout):
    log.info(f'{site} title {title!r} lookup')
    url = f'{site}/search?ask=%s' % urllib.parse.quote(title.encode('utf8'))
    log.info(url)

    result = {}

    try: 
        with urllib.request.urlopen(url, data=None, timeout=timeout) as response:
            page = html.fromstring(response.read())
            page.make_links_absolute(site, resolve_base_href=True)

            title_expected = title.strip().lower()

            for book_link in page.xpath("//div[contains(@class, 'content')]/li/a[contains(@href,'/b/')]"):
                book_url = book_link.attrib.get('href')
                book_title = book_link.text_content()
                if book_title.strip().lower().startswith(title_expected):
                    result[book_url] = book_title
    finally:
        return result

def fetch_book_info(log, url, title, timeout=default_timeout):
    result = {}

    try:
        log.info(f'fetching book info {title!r} from url {url!r}')
        with urllib.request.urlopen(url, data=None, timeout=timeout) as response:
            page = html.fromstring(response.read())
            page.make_links_absolute(site, resolve_base_href=True)

            tags = []
            series = u''
            title = u''
            translators = []
            series_index = 0
            authors = []
            cover = ''
            annotation = ''

            for i in page.xpath('//ol/li/a/text()'):
                tags.append(str(i))

            for i in page.xpath(u"//div[@class='_ga1_on_']/br[position()=1]/preceding-sibling::a[contains(@href,'/a/')]/text()"):
                authors.append(str(i))

            for i in page.xpath(u"//div[@class='_ga1_on_']/br[position()=1]/preceding-sibling::a[preceding::text()[contains(.,'перевод:')]]/text()"):
                translators.append(str(i))

            for i in page.xpath("(//div[@class='_ga1_on_']/div[@id='z0']/following-sibling::text())[1]"):
                title += i

            for i in page.xpath("//div[@class='_ga1_on_']/h8"):
                series = i.text_content()
                series_index = re.findall('\\d+', i.tail)[0]

            for i in page.xpath("./a[contains(@href,'/s/')]/text()"):
                tags.append(str(i))

            for i in page.xpath("//div[@class='genre']/a/@href"):
                tags.append(str(i.split('/')[-1]))

            for i in page.xpath(u"//div[@class='_ga1_on_']/img[1]"):
                cover = i.attrib.get('src')

            for i in page.xpath(u"//div[@class='_ga1_on_']/p[1]/text()"):
                annotation = str(i)

            title = title.rstrip("[({").strip()
            log.info(u'Found %s/%s: %s' % (series, series_index, title))

            if tags and series in tags:
                tags.remove(series)

            if translators:
                for t in translators:
                    if t in authors:
                        authors.remove(t)

            result['title'] = title
            result['authors'] = authors

            if tags:
                result['tags'] = tags
            if series != '':
                result['series'] = series
            if series_index != 0:
                result['series_index'] = series_index
            if cover != '':
                result['cover'] = cover
            if annotation != '':
                result['annotation'] = annotation

    finally: 
        return result

def main():
    logging.basicConfig(level=logging.INFO)
    logging.info('Started')

    search_criteria = 'анатомия истории'

    result = search(logging, None,  search_criteria)

    logging.info(result)

    logging.info('Finished')


if __name__ == '__main__':
    main()
